package ru.tsc.panteleev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataJsonFasterXmlSaveRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;

@Component
public class DataJsonFasterXmlSaveListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data in json file";

    @NotNull
    private static final String NAME = "data-save-json-fasterxml";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonFasterXmlSaveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showDescription();
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonFasterXmlSaveRequest(getToken()));
    }

}
