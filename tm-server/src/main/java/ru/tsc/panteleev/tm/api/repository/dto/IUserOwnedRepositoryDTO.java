package ru.tsc.panteleev.tm.api.repository.dto;

import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends IRepositoryDTO<M> {

}
