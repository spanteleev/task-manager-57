package ru.tsc.panteleev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.panteleev.tm.api.repository.model.IUserRepository;
import ru.tsc.panteleev.tm.api.service.model.IUserOwnedService;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    protected abstract IUserOwnedRepository<M> getRepository();

    @NotNull
    protected IUserRepository getUserRepository() {
        return context.getBean(IUserRepository.class);
    }

}
